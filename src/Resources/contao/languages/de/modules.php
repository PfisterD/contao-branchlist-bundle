<?php
// Backend Content Module
$GLOBALS['TL_LANG']['MOD']['branchlist'] = ['Niederlassungen', 'Niederlassungen verwalten'];
$GLOBALS['TL_LANG']['MOD']['privacy'] = ['Datenschutz', 'Datenschutzverwaltung'];

$GLOBALS['TL_LANG']['tl_privacy']['privacy_domain'] = ['Domain', 'Domain-Adresse'];
$GLOBALS['TL_LANG']['tl_privacy']['privacy_ip'] = ['IP','IP-Adresse'];
$GLOBALS['TL_LANG']['tl_privacy']['privacy_website'] = ['Webseite','Webseite'];
$GLOBALS['TL_LANG']['tl_privacy']['privacy_setting'] = ['Datenschutz', 'Datenschutz-Einstellung'];
$GLOBALS['TL_LANG']['tl_privacy']['show'] = ['Anzeigen', 'Datensatz anzeigen'];

$GLOBALS['TL_LANG']['tl_privacy']['timestamp'] = ['Zeitstempel', 'Datum und Uhrzeit'];

// Frontend modules
$GLOBALS['TL_LANG']['FMD']['branchlist'] = ['Hello World', 'Gibt den Text "Hello World" aus'];


$GLOBALS['TL_LANG']['CTE']['contact_boxes'] = 'Kontakt-Boxen';
$GLOBALS['TL_LANG']['CTE']['branchlist_card'] = 'Karten';

$GLOBALS['TL_LANG']['CTE']['road'] = 'Kontakt-Box Road-Service';
$GLOBALS['TL_LANG']['CTE']['airandsea'] = 'Kontakt-Box Air & Sea-Service';
$GLOBALS['TL_LANG']['CTE']['logistic'] = 'Kontakt-Box Logistic-Service';
$GLOBALS['TL_LANG']['CTE']['career'] = 'Kontakt-Box Karriere';
$GLOBALS['TL_LANG']['CTE']['locationcard'] = 'Standort-Karte';

$GLOBALS['TL_LANG']['tl_branchlist']['address_information'] = 'Adress-Informationen';
$GLOBALS['TL_LANG']['tl_branchlist']['communication_information'] = 'Kontakt-Informationen';
$GLOBALS['TL_LANG']['tl_branchlist']['devision_information'] = 'Gesch&auml;ftsbereich-Informationen';
$GLOBALS['TL_LANG']['tl_branchlist']['country_information'] = 'L&auml;nder-Information';
$GLOBALS['TL_LANG']['tl_branchlist']['logo_information'] = 'Niederlassungs-Logo-Information';
$GLOBALS['TL_LANG']['tl_branchlist']['alias_information'] = 'Alias Informationen';
$GLOBALS['TL_LANG']['tl_branchlist']['view_settings_contacts'] = 'Anzeige Einstellungen für Kontaktseite';
$GLOBALS['TL_LANG']['tl_branchlist']['view_settings_locactionmap'] = 'Anzeige Einstellungen für Standortkarte';
$GLOBALS['TL_LANG']['tl_branchlist']['view_settings_trainingscenter'] = 'Anzeige Einstellungen für Ausbildungskarte';


$GLOBALS['TL_LANG']['tl_branchlist']['ndl_name'][0] = 'Name';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_name'][1] = 'Geben Sie hier den Namen der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_addon'][0] = 'Zusatz';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_addon'][1] = 'Geben Sie hier einen ggf. vorhanden Namens-Zusatz der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_str'][0] = 'Stra&szlig;e';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_str'][1] = 'Geben Sie hier die Stra&szlig;e der Niederlassung ein.';
 
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_zip'][0] = 'Postleitzahl';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_zip'][1] = 'Geben Sie hier die Postleitzahl der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_city'][0] = 'Ort';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_city'][1] = 'Geben Sie hier den Ort der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_tel'][0] = 'Telefon-Nr:';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_tel'][1] = 'Geben Sie hier die Telefonnummer der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_fax'][0] = 'Fax-Nr:';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_fax'][1] = 'Geben Sie hier die Fax-Nr der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_customfield1'][0] = 'Bezeichnung (Custome-Field):';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_customfield1'][1] = 'Geben Sie hier die Bezeichnung des Benutzerdefinierten Feldes ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_customfield2'][0] = 'Wert (Custome-Field):';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_customfield2'][1] = 'Geben Sie hier denn Wert ein welchen das Benutzerdefinierte Feld enthalten soll.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_email'][0] = 'E-Mail Adresse';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_email'][1] = 'Geben Sie hier die Standard-Email Adresse der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_devision'][0] = 'Gesch&auml;ftsbereich';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_devision'][1] = 'Geben Sie hier den Gesch&auml;ftsbereich der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_country'][0] = 'Land (DE)';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_country'][1] = 'Geben Sie hier das Land der Niederlassung ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_country_en'][0] = 'Land (EN)';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_country_en'][1] = 'Geben Sie hier das Land der Niederlassung in Englisch ein.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_website'][0] = 'Website';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_website'][1] = 'Geben Sie hier die Website der Niederlassung ein.';


$GLOBALS['TL_LANG']['tl_branchlist']['ndl_logo'][0] = 'Niederlassungs-Logo';



$GLOBALS['TL_LANG']['tl_branchlist']['new'][0] = 'Neue Niederlassung';
$GLOBALS['TL_LANG']['tl_branchlist']['new'][1] = 'Eine neue Niederlassung anlegen';

$GLOBALS['TL_LANG']['tl_branchlist']['edit'][0] = 'Niederlassung bearbeiten';
$GLOBALS['TL_LANG']['tl_branchlist']['edit'][1] = 'Niederlassung mit der ID %s bearbeiten';

$GLOBALS['TL_LANG']['tl_branchlist']['copy'][0] = 'Niederlassung kopieren';
$GLOBALS['TL_LANG']['tl_branchlist']['copy'][1] = 'Niederlassung mit der ID %s kopieren';

$GLOBALS['TL_LANG']['tl_branchlist']['delete'][0] = 'Niederlassung l&ouml;schen';
$GLOBALS['TL_LANG']['tl_branchlist']['delete'][1] = 'Niederlassung mit der ID %s l&ouml;schen';
 
$GLOBALS['TL_LANG']['tl_branchlist']['show'][0] = 'Niederlassungs-Details';
$GLOBALS['TL_LANG']['tl_branchlist']['show'][1] = 'Details der Niederlassung mit der ID %s anzeigen';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_logo'][0] = 'Niederlassungs-Logo anzeigen';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_logo'][1] = 'Legen Sie hier fest, ob das Logo der Niederlassung angezeigt wird oder nicht.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_logo']['yes'] = 'Niederlassungs-Logo anzeigen';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_logo']['no'] = 'Niederlassungs-Logo nicht anzeigen';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_contacts'][0] = 'Adresse in der Kontakt-Seite';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_contacts'][1] = 'Legen Sie hier fest, ob die Niederlassung auf der Kontakte-Seite gelistet wird oder nicht';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_contacts']['yes'] = 'Anzeigen';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_contacts']['no'] = 'Verbergen';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_location_map']['yes'] = 'Anzeigen';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_location_map']['no'] = 'Verbergen';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_location_map'][0] = 'Adresse in der Standortkarte';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_location_map'][1] = 'Legen Sie hier fest, ob die Niederlassung auf der Standortkarte angezeigt wird oder nicht';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_trainingcenter_map']['yes'] = 'Anzeigen';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_trainingcenter_map']['no'] = 'Verbergen';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_trainingcenter_map'][0] = 'Adresse in der Ausbildungskarte';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_trainingcenter_map'][1] = 'Legen Sie hier fest, ob die Niederlassung auf der Standortkarte angezeigt wird oder nicht';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_trainingcenter_content'][0] = 'Zusätzlicher Text-Content auf der Ausbildungskarte';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_trainingcenter_content'][1] = 'Legen Sie hier fest, was neben der Adresse zusätzlich auf der Ausbildungskarte angezeigt wird';



$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_road'][0] = 'Alias für Kontakt-Seite im Bereich Road';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_road'][1] = 'Geben Sie hier den Alias Namen an, der auf der Kontakte-Seite anstatt des Ortes angezeigt werden soll.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_logistics'][0] = 'Alias für Kontakt-Seite im Bereich Logistics';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_logistics'][1] = 'Geben Sie hier den Alias Namen an, der auf der Kontakte-Seite anstatt des Ortes angezeigt werden soll.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_air_and_sea'][0] = 'Alias für Kontakt-Seite im Bereich Air & Sea';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_air_and_sea'][1] = 'Geben Sie hier den Alias Namen an, der auf der Kontakte-Seite anstatt des Ortes angezeigt werden soll.';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_career'][0] = 'Alias für Kontakt-Seite im Bereich Karriere';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_career'][1] = 'Geben Sie hier den Alias Namen an, der auf der Kontakte-Seite anstatt des Ortes angezeigt werden soll.';

$GLOBALS['TL_LANG']['tl_branchlist']['road'] = 'Road Services';
$GLOBALS['TL_LANG']['tl_branchlist']['logistic'] = 'Logistics Services';
$GLOBALS['TL_LANG']['tl_branchlist']['airsea'] = 'Air & Sea Services';
$GLOBALS['TL_LANG']['tl_branchlist']['career'] = 'Karriere';
$GLOBALS['TL_LANG']['tl_branchlist']['trainingcenter'] = 'Ausbildungsbetrieb';

$GLOBALS['TL_LANG']['tl_branchlist']['ndl_latitude'][0] = 'Breitengrad';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_latitude'][1] = 'Nur erforderlich, wenn nach dem Speichern nichts gefunden wurde!';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_longitude'][0] = 'Längengrad';
$GLOBALS['TL_LANG']['tl_branchlist']['ndl_longitude'][1] = 'Nur erforderlich, wenn nach dem Speichern nichts gefunden wurde!';

$GLOBALS['TL_LANG']['tl_branchlist']['geis.png'] = '<img src="bundles/contaobranchlist/assets/images/logos/geis.png" style="position: relative;top: 27px;" width="80">';
$GLOBALS['TL_LANG']['tl_branchlist']['lechner.png']  = '<img src="bundles/contaobranchlist/assets/images/logos/lechner.png" style="position:relative;top:22px;left:-8px;" width="140">';
$GLOBALS['TL_LANG']['tl_branchlist']['bischoff.png'] = '<img src="bundles/contaobranchlist/assets/images/logos/bischoff.png" style="position:relative;top:6px;padding-top:21px;" width="160">';
$GLOBALS['TL_LANG']['tl_branchlist']['sdv.png'] = '<img src="bundles/contaobranchlist/assets/images/logos/sdv.png" style="position:relative;top:6px;padding-top:21px;" width="160">';
$GLOBALS['TL_LANG']['tl_branchlist']['gt.png'] = '<img src="bundles/contaobranchlist/assets/images/logos/gt.png" style="position:relative;top:6px;padding-top:21px;" width="70">';
$GLOBALS['TL_LANG']['tl_branchlist']['tas.png'] = '<img src="bundles/contaobranchlist/assets/images/logos/tas.png" style="position:relative;top:6px;padding-top:21px;" width="70">';


$GLOBALS['TL_LANG']['tl_branchlist']['view_tel_in_contacts'][0] = 'Zeige Telefon-Nummer';
$GLOBALS['TL_LANG']['tl_branchlist']['view_tel_in_contacts'][1] = 'Gibt an, ob die Telefon-Nummer dieser Niederlassung auf der Kontaktseite angezeigt wird.';
$GLOBALS['TL_LANG']['tl_branchlist']['view_fax_in_contacts'][0] = 'Zeige Fax-Nummer';
$GLOBALS['TL_LANG']['tl_branchlist']['view_fax_in_contacts'][1] = 'Gibt an, ob die Fax-Nummer dieser Niederlassung auf der Kontaktseite angezeigt wird.';

$GLOBALS['TL_LANG']['tl_branchlist']['tel_alias_contacts'][0] = 'Anzuzeigender Telefon-Alias';
$GLOBALS['TL_LANG']['tl_branchlist']['tel_alias_contacts'][1] = 'Gibt denn Alias an, der anstatt der Telefon-Nummer dargestellt wird.';

$GLOBALS['TL_LANG']['tl_branchlist']['fax_alias_contacts'][0] = 'Anzuzeigender Telefon-Alias';
$GLOBALS['TL_LANG']['tl_branchlist']['fax_alias_contacts'][1] = 'Gibt denn Alias an, der anstatt der Telefon-Nummer dargestellt wird.';

$GLOBALS['TL_LANG']['tl_branchlist']['view_tel_in_location_map'][0] = 'Zeige Telefon-Nummer';
$GLOBALS['TL_LANG']['tl_branchlist']['view_tel_in_location_map'][1] = 'Gibt an, ob die Telefon-Nummer dieser Niederlassung auf der Standortkarte angezeigt wird.';
$GLOBALS['TL_LANG']['tl_branchlist']['view_fax_in_location_map'][0] = 'Zeige Fax-Nummer';
$GLOBALS['TL_LANG']['tl_branchlist']['view_fax_in_location_map'][1] = 'Gibt an, ob die Fax-Nummer dieser Niederlassung auf der Standortkarte angezeigt wird.';

$GLOBALS['TL_LANG']['tl_branchlist']['tel_alias_location_map'][0] = 'Anzuzeigender Telefon-Alias';
$GLOBALS['TL_LANG']['tl_branchlist']['tel_alias_location_map'][1] = 'Gibt denn Alias an, der anstatt der Telefon-Nummer dargestellt wird.';

$GLOBALS['TL_LANG']['tl_branchlist']['fax_alias_location_map'][0] = 'Anzuzeigender Telefon-Alias';
$GLOBALS['TL_LANG']['tl_branchlist']['fax_alias_location_map'][1] = 'Gibt denn Alias an, der anstatt der Telefon-Nummer dargestellt wird.';





