<?php

// Add palette to tl_module
//$GLOBALS['TL_DCA']['tl_module']['palettes']['branchlist'] = '{title_legend},name,headline,type;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
$GLOBALS['TL_DCA']['tl_branchlist'] = array
(
	// Config
	'config'   => array
	(
		'dataContainer'    => 'Table',
		'enableVersioning' => true,
		'onsubmit_callback' => array
        (
            array('tl_branchlist', 'getLatLon')
        ), 
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary'
			)
		),
	),
	// List
	'list'     => array
	(
		'sorting'           => array
		(
			'mode'        => 1,
			'fields'      => array('ndl_city'),
			'flag'        => 11,
			'panelLayout' => 'filter,sort;search'
		),
		'label' => array
		(
			'fields' => array('ndl_name', 'ndl_city', 'ndl_devision'),
			'format' => '%s<br/>%s <span style="float:right; font-weight: bold;">%s</span>',
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'      => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'       => 'act=select',
				'class'      => 'header_edit_all',
				'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations'        => array
		(
			'edit'   => array
			(
				'label' => &$GLOBALS['TL_LANG']['tl_branchlist']['edit'],
				'href'  => 'act=edit',
				'icon'  => 'edit.gif'
			),
      'copy' => array
      (
        'label'               => &$GLOBALS['TL_LANG']['tl_branchlist']['copy'],
        'href'                => 'act=copy',
        'icon'                => 'copy.gif'
      ),
			'delete' => array
			(
				'label'      => &$GLOBALS['TL_LANG']['tl_branchlist']['delete'],
				'href'       => 'act=delete',
				'icon'       => 'delete.gif',
				'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'show'   => array
			(
				'label'      => &$GLOBALS['TL_LANG']['tl_branchlist']['show'],
				'href'       => 'act=show',
				'icon'       => 'show.gif',
				'attributes' => 'style="margin-right:3px"'
			),
		)
	),
// Palettes
	'palettes' => array
	(
		
		'default'       => '{address_information},ndl_devision,ndl_name,ndl_addon,ndl_str,ndl_zip,ndl_city,ndl_country,ndl_country_en,ndl_tel,ndl_fax,ndl_customfield1,ndl_customfield2,ndl_email,ndl_website,ndl_latitude,ndl_longitude,ndl_logo,ndl_show_logo;
							{view_settings_contacts},ndl_show_in_contacts,view_tel_in_contacts,view_fax_in_contacts,tel_alias_contacts,fax_alias_contacts,ndl_contacts_alias_name_road,ndl_contacts_alias_name_logistics,ndl_contacts_alias_name_air_and_sea,ndl_contacts_alias_name_career;
							{view_settings_locactionmap},ndl_show_in_location_map,view_tel_in_location_map,view_fax_in_location_map,tel_alias_location_map,fax_alias_location_map;
							{view_settings_trainingscenter},ndl_show_in_trainingcenter_map,ndl_trainingcenter_content;'
	),	

// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql' => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql' => "int(10) unsigned NOT NULL default '0'"
		),
		'ndl_name'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_name'],
			'inputType' => 'text',
			'exclude'   => true,
			'sorting'   => true,
			'flag'      => 1,
			'search'    => true,
			'eval'      => array(
				'mandatory' => true,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),
        'ndl_addon'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_addon'],
			'inputType' => 'text',
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),
        'ndl_str'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_str'],
			'inputType' => 'text',
			'search'    => true,
			'sorting'   => false,
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => true,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),
        'ndl_zip'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_zip'],
			'inputType' => 'text',
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => true,
				'unique'    => false,
				'maxlength' => 10,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(10) NOT NULL default ''"
		),
        'ndl_city'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_city'],
			'inputType' => 'text',
      'search'    => true,
      'sorting'   => false,
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => true,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(255) NOT NULL default ''"
		),
        'ndl_tel'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_tel'],
			'inputType' => 'text',
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => true,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(255) NOT NULL default ''"
		),
        'ndl_fax'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_fax'],
			'inputType' => 'text',
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(255) NOT NULL default ''"
		),
		'ndl_customfield1'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_customfield1'],
			'inputType' => 'text',
			'search'    => false,
			'sorting'   => false,
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(255) NOT NULL default ''"
		),		
		'ndl_customfield2'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_customfield2'],
			'inputType' => 'text',
			'search'    => false,
			'sorting'   => false,
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(255) NOT NULL default ''"
		),	
        'ndl_email'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_email'],
			'inputType' => 'text',
			'search'    => false,
			'sorting'   => false,     
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => true,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "text NULL"
		),
		'ndl_devision'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_devision'],
			'inputType' => 'checkbox',
			'options'   => array('road', 'logistic', 'airsea', 'career', 'trainingcenter'),
			'reference' => &$GLOBALS['TL_LANG']['tl_branchlist'],
			'search'    => false,
			'sorting'   => false,      
			'eval'      => array(
				'mandatory'    => true,
				'multiple'     => true,
				'tl_class'     => '',
			),
			'sql'       => "text NULL"
		),
        'ndl_country'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_country'],
			'inputType' => 'text',
				'search'    => false,
			'sorting'   => false,      
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => true,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(255) NOT NULL default ''"
		),
		'ndl_country_en'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_country_en'],
			'inputType' => 'text',
			'search'    => false,
			'sorting'   => false,      
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => true,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(255) NOT NULL default ''"
		),
        'ndl_website'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_website'],
			'inputType' => 'text',
			'search'    => false,
			'sorting'   => false,      
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),			
			'sql'       => "varchar(255) NOT NULL default ''"
		),
		'ndl_logo'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_logo'],
			'inputType' => 'radio',
			'options'   => array('geis.png', 'lechner.png', 'bischoff.png', 'gt.png', 'tas.png'),
			'reference' => &$GLOBALS['TL_LANG']['tl_branchlist'],
			'search'    => false,
			'sorting'   => false,      
			'eval'      => array(
				'mandatory'    => true,
				'multiple'     => false,
				'tl_class'     => 'clr w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),
		'ndl_show_logo' => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_logo'],
			'inputType' => 'select',
			'options'   => array('yes', 'no'),
			'reference' => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_logo'],
			'search'    => false,
			'sorting'   => false,      
			'eval'      => array(
				'mandatory'    => false,
				'multiple'     => false,
				'tl_class'     => 'w50',
			),
			'sql'       => "varchar(3) NOT NULL default ''"
		),
		'ndl_contacts_alias_name_road'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_road'],
			'inputType' => 'text',
			'search'    => false,
			'sorting'   => false,      
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),		
		'ndl_contacts_alias_name_logistics'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_logistics'],
			'inputType' => 'text',
			'search'    => false,
			'sorting'   => false,      
			'exclude'   => true,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),	
		'ndl_contacts_alias_name_air_and_sea'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_air_and_sea'],
			'inputType' => 'text',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),	
		'ndl_contacts_alias_name_career'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_contacts_alias_name_career'],
			'inputType' => 'text',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),
		'ndl_show_in_contacts' => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_contacts'],
			'inputType' => 'select',
			'options'   => array('yes', 'no'),
			'reference' => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_contacts'],
			'search'    => false,
			'sorting'   => false,      
			'eval'      => array(
				'mandatory'    => false,
				'multiple'     => false,
				'tl_class'     => '',
			),
			'sql'       => "varchar(3) NOT NULL default ''"
		),	
		'ndl_show_in_location_map' => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_location_map'],
			'inputType' => 'select',
			'options'   => array('yes', 'no'),
			'reference' => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_location_map'],
			'search'    => false,
			'sorting'   => false,      
			'eval'      => array(
				'mandatory'    => false,
				'multiple'     => false,
				'tl_class'     => '',
			),
			'sql'       => "varchar(3) NOT NULL default ''"
		),		
		'ndl_show_in_trainingcenter_map' => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_trainingcenter_map'],
			'inputType' => 'select',
			'options'   => array('yes', 'no'),
			'reference' => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_show_in_trainingcenter_map'],
			'search'    => false,
			'default' 	=> 'no',
			'sorting'   => false,      
			'eval'      => array(
				'mandatory'    => false,
				'multiple'     => false,
				'tl_class'     => '',
			),
			'sql'       => "varchar(3) NOT NULL default ''"
		),		
		'ndl_trainingcenter_content' => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_trainingcenter_content'],
			'inputType' => 'textarea',
			'reference' => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_trainingcenter_content'],
			'search'    => false,  
			'exclude'   => true,
			'eval'     => ['rte'=>'tinyMCE', 'tl_class'=>'clr'],
			'sql'       => 'text NOT NULL'
		),		
		'ndl_latitude' => array
		(			
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_latitude'],
			'inputType' => 'text',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(10) NOT NULL default ''"
		),
		'ndl_longitude' => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['ndl_longitude'],
			'inputType' => 'text',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,
			'eval'      => array(
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(10) NOT NULL default ''"
		),
		'view_tel_in_contacts'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['view_tel_in_contacts'],
			'inputType' => 'checkbox',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,      
			'default'	=> true,
			'eval'      => array(
				'tl_class' => 'w50 m12'
			),
			'sql'       => "char(1) NOT NULL default ''"
		),
		'view_fax_in_contacts'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['view_fax_in_contacts'],
			'inputType' => 'checkbox',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,      
			'default'	=> true,
			'eval'      => array(
				'tl_class' => 'w50 m12'
			),
			'sql'       => "char(1) NOT NULL default ''"
		),
		'tel_alias_contacts'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['tel_alias_contacts'],
			'inputType' => 'text',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,
			'eval'      => array(
				'disabled'	=> false,
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),
		'fax_alias_contacts'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['fax_alias_contacts'],
			'inputType' => 'text',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,
			'eval'      => array(
				'disabled'	=> false,
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),
		'view_tel_in_location_map'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['view_tel_in_location_map'],
			'inputType' => 'checkbox',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,      
			'default'	=> true,
			'eval'      => array(
				'tl_class' => 'w50 m12'
			),
			'sql'       => "char(1) NOT NULL default ''"
		),
		'view_fax_in_location_map'    => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['view_fax_in_location_map'],
			'inputType' => 'checkbox',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,      
			'default'	=> true,
			'eval'      => array(
				'tl_class' => 'w50 m12'
			),
			'sql'       => "char(1) NOT NULL default ''"
		),
		'tel_alias_location_map'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['tel_alias_location_map'],
			'inputType' => 'text',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,
			'eval'      => array(
				'disabled'	=> false,
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		),
		'fax_alias_location_map'  => array
		(
			'label'     => &$GLOBALS['TL_LANG']['tl_branchlist']['fax_alias_location_map'],
			'inputType' => 'text',
			'exclude'   => true,
			'search'    => false,
			'sorting'   => false,
			'eval'      => array(
				'disabled'	=> false,
				'mandatory' => false,
				'unique'    => false,
				'maxlength' => 255,
				'tl_class'  => 'w50',
			),
			'sql'       => "varchar(255) NOT NULL default ''"
		)		
		
		
    )
);	


class tl_branchlist extends Backend
{
	/**
	 * GetLatLong from Open Street Maps
	 */
	public function getLatLon($dc)
	{
		if ($this->Input->post('ndl_latitude') == "" and $this->Input->post('ndl_latitude') == "") {
			$address = $this->Input->post('ndl_str').', '.$this->Input->post('ndl_city').', '.$this->Input->post('ndl_country'); // Google HQ
			$prepAddr = urlencode($address);
			//$geocode = file_get_contents("http://www.mapquestapi.com/geocoding/v1/address?key=rT8k7Ezk12PJUOsWtgUlNUEVO4JYVSvb&location=".$prepAddr);
			$geocode = file_get_contents("https://nominatim.openstreetmap.org/search?format=json&email=info@geis-group.de&q=".$prepAddr);

			$output = json_decode($geocode);
			//$latitude = $output->results[0]->locations[0]->latLng->lat;
			//$longitude = $output->results[0]->locations[0]->latLng->lng;
			$latitude = $output[0]->lat;
			$longitude = $output[0]->lon;
			
			$dc->Database->prepare("UPDATE tl_branchlist SET tstamp=?, ndl_latitude=?, ndl_longitude=? WHERE id=?")
							   ->execute(time(), $latitude, $longitude, $dc->activeRecord->id);
			
			
		}
	}
}
