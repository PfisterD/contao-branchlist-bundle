<?php

// Add palette to tl_module
//$GLOBALS['TL_DCA']['tl_module']['palettes']['branchlist'] = '{title_legend},name,headline,type;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
$GLOBALS['TL_DCA']['tl_privacy'] = [
	// Config
	'config' => [
		'dataContainer' => 'Table',
		'enableVersioning' => true,
		'closed' => true,
		'notEditable' => true,
		'notCopyable' => true,
		'sql' => [
			'keys' => [
				'id' => 'primary'
			]
		],
	],
	// List
	'list' => [
		'sorting' => [
			'mode' => 1,
			'fields' => ['privacy_website'],
			'headerFields' => ['privacy_website'],
			'flag' => 11,
			'panelLayout' => 'filter,sort;search'
		],
		'label' =>[
			'fields' => ['privacy_ip'],
			'format' => '%s',
			'label_callback' => ['tl_privacy', 'lst']
		],
		'global_operations' => [],
		'operations' => [
			'delete' => [
				'label'      => &$GLOBALS['TL_LANG']['tl_privacy']['delete'],
				'href'       => 'act=delete',
				'icon'       => 'delete.gif',
				'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			],
			'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_privacy']['show'], 
                'href'  => 'act=show', 
                'icon'  => 'show.gif' 
            ]
		]
	],
// Palettes
	'palettes' => [		
		'default' => ''
	],	
// Fields
	'fields' => [
		'id' => [
			'sql' => "int(10) unsigned NOT NULL auto_increment"
		],
		'timestamp' => [
			'label' => &$GLOBALS['TL_LANG']['tl_privacy']['timestamp'],
			'sql' => "int(10) unsigned NOT NULL default CURRENT_TIMESTAMP"
		],
		'privacy_domain' => [
			'label' => &$GLOBALS['TL_LANG']['tl_privacy']['privacy_domain'],
			'search' => true,
			'sorting' => true,
			'inputType' => 'text',
			'eval' => ['mandatory' => true, 'maxlength' => 255],
			'sql' => "varchar(255) NOT NULL default ''"
		],
		'privacy_ip' => [
			'label' => &$GLOBALS['TL_LANG']['tl_privacy']['privacy_ip'],
			'search' => true,
			'sorting' => true,
			'inputType' => 'text',
			'eval' => ['mandatory' => true, 'maxlength' => 39],
			'sql' => "varchar(39) NOT NULL default ''"
		],
        'privacy_setting' => [
			'label' => &$GLOBALS['TL_LANG']['tl_privacy']['privacy_setting'],
			'search' => true,
			'sorting' => true,
			'inputType' => 'text',
			'eval' => ['mandatory' => true, 'maxlength' => 1],
			'sql' => "varchar(1) NOT NULL default ''"
		],
		'privacy_website' => [
			'label' => &$GLOBALS['TL_LANG']['tl_privacy']['privacy_website'],
			'search' => true,
			'sorting' => true,
			'flag' => 4,
			'length' => 255,
			'inputType' => 'text',
			'eval' => ['mandatory' => true, 'maxlength' => 255],
            'sql' => "varchar(255) NOT NULL default ''",
		],
    ]
];	

class tl_privacy extends Backend {
  public function lst($row, $label)
	{
		$erg="<div>";
		$erg=$erg."<div syle='float:left;'>IP-Adresse: <strong>" . $row['privacy_ip'] . "</strong><br/>Datum und Uhrzeit: ".date('d.m.Y H:i', $row['timestamp'])."</div>";
		if($row['privacy_setting'] == '1'){
			$erg=$erg."<div syle='float:right;'><br/>Einstellung: <Strong>Erforderlich</strong></div>";
		}elseif($row['privacy_setting'] == '2'){
			$erg=$erg."<div syle='float:right;'><br/>Einstellung: <Strong>Komfort</strong></div>";
		}elseif($row['privacy_setting'] == '3'){
			$erg=$erg."<div syle='float:right;'><br/>Einstellung: <Strong>Statistik</strong></div>";
		}
		$erg=$erg."</div>";
		return $erg;
	}	
}

