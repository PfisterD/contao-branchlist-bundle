<?php
/**
 * Constants
 */
define('BRANCHLIST_PATH', 'bundles/contaobranchlist');
define('BRANCHLIST_VERSION', '1.0.2');

$GLOBALS['BE_MOD']['content']['branchlist'] = array(
	'tables' => array('tl_branchlist'),
	'icon'   => BRANCHLIST_PATH.'/assets/images/company.png'
);

$GLOBALS['BE_MOD']['system']['privacy'] = array(
	'tables' => array('tl_privacy')
);

/**
 * Content elements
 */
$GLOBALS['TL_CTE']['contact_boxes']['road'] = 'Pfister\ContaoBranchlistBundle\Classes\contao\elements\CE_Road';
$GLOBALS['TL_CTE']['contact_boxes']['airandsea'] = 'Pfister\ContaoBranchlistBundle\Classes\contao\elements\CE_AirAndSea';
$GLOBALS['TL_CTE']['contact_boxes']['logistic'] = 'Pfister\ContaoBranchlistBundle\Classes\contao\elements\CE_Logistic';
$GLOBALS['TL_CTE']['contact_boxes']['career'] = 'Pfister\ContaoBranchlistBundle\Classes\contao\elements\CE_Career';

$GLOBALS['TL_CTE']['branchlist_card']['locationcard'] = 'Pfister\ContaoBranchlistBundle\Classes\contao\elements\CE_LocationCard';
$GLOBALS['TL_CTE']['branchlist_card']['trainingcenters'] = 'Pfister\ContaoBranchlistBundle\Classes\contao\elements\CE_TrainingCenters';


/**
 *  Custome CSS
 */
$GLOBALS['TL_CSS'][] = BRANCHLIST_PATH.'/assets/html/css/style.css';
$GLOBALS['TL_CSS'][] = BRANCHLIST_PATH.'/assets/html/css/jquery-jvectormap.css';




// Frontend modules
//$GLOBALS['FE_MOD']['miscellaneous']['branchlist'] = 'Pfister\ContaoBranchlistBundle\Module\BranchlistModule';