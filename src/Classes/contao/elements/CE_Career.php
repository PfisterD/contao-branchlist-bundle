<?php

namespace Pfister\ContaoBranchlistBundle\Classes\contao\elements;

class CE_Career extends \ContentElement
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_career';

	public function generate()
	{
		if (TL_MODE == 'BE')
		{
						
			$this->Template = new \BackendTemplate('be_wildcard');
			$this->Template->wildcard = '<img src="bundles/contaobranchlist/assets/images/career.png" width="50" height="50" alt="">';
			$this->Template->title = "Career Services Box";
			
			return $this->Template->parse();
		}
		
		return parent::generate();
	}
	/**
	 * Generate the content element
	 */
	protected function compile()
	{
		$intCity = $this->Input->get('CareerNDL');
		$objCitys = $this->Database->prepare("SELECT CASE WHEN ndl_contacts_alias_name_career <> '' THEN ndl_contacts_alias_name_career ELSE ndl_city END AS 'CITY' FROM tl_branchlist WHERE ndl_devision LIKE ? AND ndl_show_in_contacts = ?  ORDER BY CITY ASC")->execute('%career%', 'yes');
		
		$objCareerLocations = $this->Database->prepare("SELECT * FROM tl_branchlist WHERE ndl_devision LIKE ? AND ndl_show_in_contacts = ? ")->execute('%career%', 'yes');
		
		
		//Orte fuer SelectBox 
		while ($objCitys->next())
		{
			$blnSelected = ($objCitys->CITY == $intCity) ? true : false;
			$arrCitys[] = array
			(
			'city' => $objCitys->CITY,
			'selected' => $blnSelected
			);
		}

		while ($objCareerLocations->next())
		{
			$CareerLocationsRst[] = array (
				'id' => $objCareerLocations->id,
				'name1' => $objCareerLocations->ndl_name,
				'name2' => $objCareerLocations->ndl_addon,
				'street' => $objCareerLocations->ndl_str,
				'zip' => $objCareerLocations->ndl_zip,
				'city' => $objCareerLocations->ndl_city,
				'country' => $objCareerLocations->ndl_country,
				'country_en' => $objCareerLocations->ndl_country_en,					
				'phone' => $objCareerLocations->ndl_tel,
				'fax' => $objCareerLocations->ndl_fax,
				'custome1' => $objCareerLocations->ndl_customfield1,
				'custome2' => $objCareerLocations->ndl_customfield2,
				'website' => $objCareerLocations->ndl_website,
				'email' => $objCareerLocations->ndl_email,
				'lat' => $objCareerLocations->ndl_latitude,
				'lon' => $objCareerLocations->ndl_longitude,
				'logo' => $objCareerLocations->ndl_logo,
				'show_logo' => $objCareerLocations->ndl_show_logo,
				'show_tel_in_location_map' =>$objCareerLocations->view_tel_in_location_map,
				'show_fax_in_location_map' =>$objCareerLocations->view_fax_in_location_map,
				'tel_alias_location_map' =>$objCareerLocations->tel_alias_location_map,
				'fax_alias_location_map' =>$objCareerLocations->fax_alias_location_map,
				'show_tel_in_contacts' =>$objCareerLocations->view_tel_in_contacts,
				'show_fax_in_contacts' =>$objCareerLocations->view_fax_in_contacts,
				'tel_alias_contacts' =>$objCareerLocations->tel_alias_contacts,
				'fax_alias_contacts' =>$objCareerLocations->fax_alias_contacts,
				'ndl_contacts_alias_name_career' =>$objCareerLocations->ndl_contacts_alias_name_career
			);
		}
		
		$this->Template->CareerLocations = json_encode($CareerLocationsRst);
		$this->Template->CitysCareer = $arrCitys;	
	}
}
