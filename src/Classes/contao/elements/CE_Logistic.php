<?php

namespace Pfister\ContaoBranchlistBundle\Classes\contao\elements;

class CE_Logistic extends \ContentElement
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_logistic';

	public function generate()
	{
		if (TL_MODE == 'BE')
		{
						
			$this->Template = new \BackendTemplate('be_wildcard');
			$this->Template->wildcard = '<img src="bundles/contaobranchlist/assets/images/logistics.png" width="50" height="50" alt="">';
			$this->Template->title = "Logistics Services Box";
			
			return $this->Template->parse();
		}
		
		return parent::generate();
	}
	/**
	 * Generate the content element
	 */
	protected function compile()
	{
		$intCity = $this->Input->get('LogisticsNDL');
		$objCitys = $this->Database->prepare("SELECT distinct CASE WHEN ndl_contacts_alias_name_logistics <> '' THEN ndl_contacts_alias_name_logistics ELSE ndl_city END AS 'CITY' FROM tl_branchlist WHERE ndl_devision LIKE ? AND ndl_show_in_contacts = ?  ORDER BY CITY ASC")->execute('%logistic%', 'yes');
		
		$objLogisticsLocations = $this->Database->prepare("SELECT * FROM tl_branchlist WHERE ndl_devision LIKE ? AND ndl_show_in_contacts = ? ")->execute('%logistic%', 'yes');
		
		//Orte fuer SelectBox
		while ($objCitys->next())
		{
			$blnSelected = ($objCitys->CITY == $intCity) ? true : false;
			$arrCitys[] = array
			(
			'city' => $objCitys->CITY,
			'selected' => $blnSelected
			);
		}
		
		while ($objLogisticsLocations->next())
		{
			$LogisticsLocationsRst[] = array (
				'id' => $objLogisticsLocations->id,
				'name1' => $objLogisticsLocations->ndl_name,
				'name2' => $objLogisticsLocations->ndl_addon,
				'street' => $objLogisticsLocations->ndl_str,
				'zip' => $objLogisticsLocations->ndl_zip,
				'city' => $objLogisticsLocations->ndl_city,
				'country' => $objLogisticsLocations->ndl_country,
				'country_en' => $objLogisticsLocations->ndl_country_en,					
				'phone' => $objLogisticsLocations->ndl_tel,
				'fax' => $objLogisticsLocations->ndl_fax,
				'custome1' => $objLogisticsLocations->ndl_customfield1,
				'custome2' => $objLogisticsLocations->ndl_customfield2,
				'website' => $objLogisticsLocations->ndl_website,
				'email' => $objLogisticsLocations->ndl_email,
				'lat' => $objLogisticsLocations->ndl_latitude,
				'lon' => $objLogisticsLocations->ndl_longitude,
				'logo' => $objLogisticsLocations->ndl_logo,
				'show_logo' => $objLogisticsLocations->ndl_show_logo,
				'show_tel_in_location_map' =>$objLogisticsLocations->view_tel_in_location_map,
				'show_fax_in_location_map' =>$objLogisticsLocations->view_fax_in_location_map,
				'tel_alias_location_map' =>$objLogisticsLocations->tel_alias_location_map,
				'fax_alias_location_map' =>$objLogisticsLocations->fax_alias_location_map,
				'show_tel_in_contacts' =>$objLogisticsLocations->view_tel_in_contacts,
				'show_fax_in_contacts' =>$objLogisticsLocations->view_fax_in_contacts,
				'tel_alias_contacts' =>$objLogisticsLocations->tel_alias_contacts,
				'fax_alias_contacts' =>$objLogisticsLocations->fax_alias_contacts,
				'ndl_contacts_alias_name_logistics' =>$objLogisticsLocations->ndl_contacts_alias_name_logistics
			);
		}
		
		$this->Template->LogisticsLocations = json_encode($LogisticsLocationsRst);
		$this->Template->CitysLogistic = $arrCitys;	
	}
}
