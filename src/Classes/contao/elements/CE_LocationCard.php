<?php

namespace Pfister\ContaoBranchlistBundle\Classes\contao\elements;

class CE_LocationCard extends \ContentElement
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_locationcard';


	/**
	 * Generate the content element
	 */
	protected function compile()
	{
		/*$objDE = $this->Database->prepare("SELECT * FROM tl_branchlist where ndl_country = ? and ndl_show_in_location_map = 'yes'")->execute('Deutschland');
		$objCZ = $this->Database->prepare("SELECT * FROM tl_branchlist where ndl_country = ? and ndl_show_in_location_map = 'yes'")->execute('Tschechien');
		$objLU = $this->Database->prepare("SELECT * FROM tl_branchlist where ndl_country = ? and ndl_show_in_location_map = 'yes'")->execute('Luxemburg');
		$objCH = $this->Database->prepare("SELECT * FROM tl_branchlist where ndl_country = ? and ndl_show_in_location_map = 'yes'")->execute('Schweiz');
		$objPL = $this->Database->prepare("SELECT * FROM tl_branchlist where ndl_country = ? and ndl_show_in_location_map = 'yes'")->execute('Polen');
		$objSK = $this->Database->prepare("SELECT * FROM tl_branchlist where ndl_country = ? and ndl_show_in_location_map = 'yes'")->execute('Slowakei');*/
		$objList = $this->Database->prepare("SELECT * FROM tl_branchlist where ndl_show_in_location_map = 'yes'")->execute();
		
		while ($objList->next())
		{
			$result[] = array (
				'id' => $objList->id,
				'name1' => $objList->ndl_name,
				'name2' => $objList->ndl_addon,
				'street' => $objList->ndl_str,
				'zip' => $objList->ndl_zip,
				'city' => $objList->ndl_city,
				'country' => $objList->ndl_country,
				'country_en' => $objList->ndl_country_en,					
				'phone' => $objList->ndl_tel,
				'fax' => $objList->ndl_fax,
				'custome1' => $objList->ndl_customfield1,
				'custome2' => $objList->ndl_customfield2,
				'website' => $objList->ndl_website,
				'email' => $objList->ndl_email,
				'lat' => $objList->ndl_latitude,
				'lon' => $objList->ndl_longitude,
				'logo' => $objList->ndl_logo,
				'show_logo' => $objList->ndl_show_logo,
				'show_tel' =>$objList->view_tel_in_location_map,
				'show_fax' =>$objList->view_fax_in_location_map,
				'tel_alias' =>$objList->tel_alias_location_map,
				'fax_alias' =>$objList->fax_alias_location_map
			);
		}
		$this->Template->Locations = json_encode($result);
		
		
	}
}