<?php

namespace Pfister\ContaoBranchlistBundle\Classes\contao\elements;

class CE_Road extends \ContentElement
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_road';

	public function generate()
	{
		if (TL_MODE == 'BE')
		{
						
			$this->Template = new \BackendTemplate('be_wildcard');
			$this->Template->wildcard = '<img src="bundles/contaobranchlist/assets/images/road.png" width="50" height="50" alt="">';
			$this->Template->title = "Road Services Box";
			
			return $this->Template->parse();
		}
		
		return parent::generate();
	}
	
	/**
	 * Generate the content element
	 */
	protected function compile()
	{
		$intCity = $this->Input->get('RoadNDL');
		$objCitys = $this->Database->prepare("SELECT distinct CASE WHEN ndl_contacts_alias_name_road <> '' THEN ndl_contacts_alias_name_road ELSE ndl_city END AS 'CITY' FROM tl_branchlist WHERE ndl_devision LIKE ? AND ndl_show_in_contacts = ?  ORDER BY CITY ASC")->execute('%road%', 'yes');
		
		$objRoadLocations = $this->Database->prepare("SELECT * FROM tl_branchlist WHERE ndl_devision LIKE ? AND ndl_show_in_contacts = ? ")->execute('%road%', 'yes');
		
		//Orte fuer SelectBox
		while ($objCitys->next())
		{
			$blnSelected = ($objCitys->CITY == $intCity) ? true : false;
			$arrCitys[] = array
			(
			'city' => $objCitys->CITY,
			'selected' => $blnSelected
			);
		}
		
		while ($objRoadLocations->next())
		{
			$RoadLocationsRst[] = array (
				'id' => $objRoadLocations->id,
				'name1' => $objRoadLocations->ndl_name,
				'name2' => $objRoadLocations->ndl_addon,
				'street' => $objRoadLocations->ndl_str,
				'zip' => $objRoadLocations->ndl_zip,
				'city' => $objRoadLocations->ndl_city,
				'country' => $objRoadLocations->ndl_country,
				'country_en' => $objRoadLocations->ndl_country_en,					
				'phone' => $objRoadLocations->ndl_tel,
				'fax' => $objRoadLocations->ndl_fax,
				'custome1' => $objRoadLocations->ndl_customfield1,
				'custome2' => $objRoadLocations->ndl_customfield2,
				'website' => $objRoadLocations->ndl_website,
				'email' => $objRoadLocations->ndl_email,
				'lat' => $objRoadLocations->ndl_latitude,
				'lon' => $objRoadLocations->ndl_longitude,
				'logo' => $objRoadLocations->ndl_logo,
				'show_logo' => $objRoadLocations->ndl_show_logo,
				'show_tel_in_location_map' =>$objRoadLocations->view_tel_in_location_map,
				'show_fax_in_location_map' =>$objRoadLocations->view_fax_in_location_map,
				'tel_alias_location_map' =>$objRoadLocations->tel_alias_location_map,
				'fax_alias_location_map' =>$objRoadLocations->fax_alias_location_map,
				'show_tel_in_contacts' =>$objRoadLocations->view_tel_in_contacts,
				'show_fax_in_contacts' =>$objRoadLocations->view_fax_in_contacts,
				'tel_alias_contacts' =>$objRoadLocations->tel_alias_contacts,
				'fax_alias_contacts' =>$objRoadLocations->fax_alias_contacts,
				'ndl_contacts_alias_name_road' =>$objRoadLocations->ndl_contacts_alias_name_road
			);
		}
		
		
		$this->Template->RoadLocations = json_encode($RoadLocationsRst);
		$this->Template->CitysRoad = $arrCitys;	
	}
}
