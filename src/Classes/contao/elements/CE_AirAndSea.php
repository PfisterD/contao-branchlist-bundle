<?php

namespace Pfister\ContaoBranchlistBundle\Classes\contao\elements;

class CE_AirAndSea extends \ContentElement
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_airandsea';

	public function generate()
	{
		if (TL_MODE == 'BE')
		{
						
			$this->Template = new \BackendTemplate('be_wildcard');
			$this->Template->wildcard = '<img src="bundles/contaobranchlist/assets/images/air_and_sea.png" width="50" height="50" alt="">';
			$this->Template->title = "Air and Sea Services Box";
			
			return $this->Template->parse();
		}
		
		return parent::generate();
	}

	/**
	 * Generate the content element
	 */
	protected function compile()
	{
		$intCity = $this->Input->get('AirAndSeaNDL');
		$objCitys = $this->Database->prepare("SELECT distinct CASE WHEN ndl_contacts_alias_name_air_and_sea <> '' THEN ndl_contacts_alias_name_air_and_sea ELSE ndl_city END AS 'CITY' FROM tl_branchlist WHERE ndl_devision LIKE ? AND ndl_show_in_contacts = ?  ORDER BY CITY ASC")->execute('%airsea%', 'yes');
		
		$objAirAndSeaLocations = $this->Database->prepare("SELECT * FROM tl_branchlist WHERE ndl_devision LIKE ? AND ndl_show_in_contacts = ? ")->execute('%airsea%', 'yes');
		
		
		//Orte fuer SelectBox
		while ($objCitys->next())
		{
			$blnSelected = ($objCitys->CITY == $intCity) ? true : false;
			$arrCitys[] = array
			(
			'city' => $objCitys->CITY,
			'selected' => $blnSelected
			);
		}
		
		while ($objAirAndSeaLocations->next())
		{
			$AirAndSeaLocationsRst[] = array (
				'id' => $objAirAndSeaLocations->id,
				'name1' => $objAirAndSeaLocations->ndl_name,
				'name2' => $objAirAndSeaLocations->ndl_addon,
				'street' => $objAirAndSeaLocations->ndl_str,
				'zip' => $objAirAndSeaLocations->ndl_zip,
				'city' => $objAirAndSeaLocations->ndl_city,
				'country' => $objAirAndSeaLocations->ndl_country,
				'country_en' => $objAirAndSeaLocations->ndl_country_en,					
				'phone' => $objAirAndSeaLocations->ndl_tel,
				'fax' => $objAirAndSeaLocations->ndl_fax,
				'custome1' => $objAirAndSeaLocations->ndl_customfield1,
				'custome2' => $objAirAndSeaLocations->ndl_customfield2,
				'website' => $objAirAndSeaLocations->ndl_website,
				'email' => $objAirAndSeaLocations->ndl_email,
				'lat' => $objAirAndSeaLocations->ndl_latitude,
				'lon' => $objAirAndSeaLocations->ndl_longitude,
				'logo' => $objAirAndSeaLocations->ndl_logo,
				'show_logo' => $objAirAndSeaLocations->ndl_show_logo,
				'show_tel_in_location_map' =>$objAirAndSeaLocations->view_tel_in_location_map,
				'show_fax_in_location_map' =>$objAirAndSeaLocations->view_fax_in_location_map,
				'tel_alias_location_map' =>$objAirAndSeaLocations->tel_alias_location_map,
				'fax_alias_location_map' =>$objAirAndSeaLocations->fax_alias_location_map,
				'show_tel_in_contacts' =>$objAirAndSeaLocations->view_tel_in_contacts,
				'show_fax_in_contacts' =>$objAirAndSeaLocations->view_fax_in_contacts,
				'tel_alias_contacts' =>$objAirAndSeaLocations->tel_alias_contacts,
				'fax_alias_contacts' =>$objAirAndSeaLocations->fax_alias_contacts,
				'ndl_contacts_alias_name_air_and_sea' =>$objAirAndSeaLocations->ndl_contacts_alias_name_air_and_sea
			);
		}
		
		$this->Template->AirAndSeaLocations = json_encode($AirAndSeaLocationsRst);
		$this->Template->CitysAirAndSea = $arrCitys;	
	}
}
