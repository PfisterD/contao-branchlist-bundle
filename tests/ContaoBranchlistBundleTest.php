<?php

/*
 * This file is part of [package name].
 *
 * (c) John Doe
 *
 * @license LGPL-3.0-or-later
 */

namespace Pfister\ContaoBranchlistBundle\Tests;

use Pfister\ContaoBranchlistBundle\ContaoBranchlistBundle;
use PHPUnit\Framework\TestCase;

class ContaoBranchlistBundleTest extends TestCase
{
    public function testCanBeInstantiated()
    {
        $bundle = new ContaoBranchlistBundle();

        $this->assertInstanceOf('Pfister\ContaoBranchlistBundle\ContaoBranchlistBundle', $bundle);
    }
}
